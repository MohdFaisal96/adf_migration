import requests

##get bearer toekn - GET

url = "https://login.microsoftonline.com/695ac491-8f55-471b-8c57-599ba36b767e/oauth2/token"

payload='grant_type=client_credentials&client_id=ae9cefe0-48b4-4f6a-8f33-424e3b6e69ff&client_secret=MLI8Q~opIOqbOHwEPLS2F53Aobz2olV5K~tX3cBi&resource=https%3A%2F%2Fmanagement.azure.com%2F'
headers = {
  'Content-Type': 'application/x-www-form-urlencoded',
  'Cookie': 'esctx=PAQABAAEAAAD--DLA3VO7QrddgJg7Wevr3AjoaT4gf4oAa8lHgahKmBybqK0WypyjdG1AHlQYSmJzqB86o1zbGTdsi2rBt9lVQkI2-BD0RBIsEyXFXORnly9kXwVglSLxpCd-BE9M-X3VjbMInywkCTODyddtvyB0Av1X0TjsygdLxKHZbxsPojm6SvTTmZWxpsULpLCT4dQgAA; fpc=Aj_kGH71pN5Nr_e0b4Z8VlodBeyCAgAAAKIh8NsOAAAAhss-ZwEAAAByIfDbDgAAAFQHo64BAAAAjCHw2w4AAAA; stsservicecookie=estsfd; x-ms-gateway-slice=estsfd'
}

response = requests.request("GET", url, headers=headers, data=payload)

data = response.json()
access_token = data.get('access_token')
# print(access_token)


##Trigger ADF pipleine -POST

url = "https://management.azure.com/subscriptions/1d5bc235-2eda-464b-b8a4-14d75743182a/resourceGroups/MnT_PoC_Dev/providers/Microsoft.DataFactory/factories/MnTADFDev/pipelines/pipeline1/createRun?api-version=2018-06-01"

payload={}
headers = {
  'Authorization': f'Bearer {access_token}'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
